defmodule RssReaderWeb.PageController do
  use RssReaderWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
