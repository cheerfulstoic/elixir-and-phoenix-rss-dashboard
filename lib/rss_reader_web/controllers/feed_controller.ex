defmodule RssReaderWeb.FeedController do
  use RssReaderWeb, :controller

  alias RssReader.Feeds
  alias RssReader.Feeds.Feed

  def index(conn, _params) do
    stories = RssReader.Feeds.all_stories()
    render(conn, "index.html", stories: stories)
  end

  def show(conn, %{"id" => id}) do
    feed = Feeds.get_feed!(id)
    render(conn, "show.html", feed: feed)
  end
end
