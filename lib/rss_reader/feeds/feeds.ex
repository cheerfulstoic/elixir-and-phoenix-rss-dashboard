defmodule RssReader.Feeds do
  def all_stories() do
    RssReader.Feeds.Slurper.all_stories()
  end
end
