defmodule RssReader.Feeds.Feed do
  use Ecto.Schema
  import Ecto.Changeset


  schema "feeds" do
    field :body, :string
    field :title, :string

    timestamps()
  end

  @doc false
  def changeset(feed, attrs) do
    feed
    |> cast(attrs, [:title, :body])
    |> validate_required([:title, :body])
  end
end
