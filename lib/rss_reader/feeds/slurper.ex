defmodule RssReader.Feeds.Slurper do
  use GenServer

  def start_link(state) do
    GenServer.start_link(__MODULE__, state, name: __MODULE__)
  end

  @impl true
  def init(urls) do
    IO.puts "Starting slurper"
    schedule_work

    stories = fetch_stories(urls)

    {:ok, %{urls: urls, stories: stories}}
  end

  # Client

  def all_stories() do
    GenServer.call(__MODULE__, :all_stories)
  end

  # Callbacks

  def handle_info(:work, state) do
    IO.puts "Doing work..."
    stories = fetch_stories(state.urls)

    schedule_work() # Reschedule once more
    {:noreply, %{state | stories: stories}}
  end

  def handle_call(:all_stories, _from, state) do
    {:reply, state.stories, state}
  end

  defp schedule_work() do
    Process.send_after(self(), :work, 60 * 1000) # In 2 hours
  end

  def fetch_stories(urls) when is_list(urls) do
    Enum.flat_map(urls, &fetch_stories(&1))
    |> Enum.sort_by(fn (t) -> t.updated end)
    |> Enum.reverse()
  end

  def fetch_stories(url) do
    %{body: body} = HTTPoison.get!(url)

    feed_data = ElixirFeedParser.parse(body)

    feed_data[:entries] |> Enum.map(fn (entry) ->
      {:ok, datetime} = Timex.parse(entry.updated, "{RFC1123}")

      entry
      |> Map.take(~w[author title content description updated]a)
      |> Map.update!(:content, fn (html) ->
        HtmlSanitizeEx.basic_html(html)
      end)
      |> Map.put(:rss_url, url)
      |> Map.put(:updated, datetime)
    end)
  end
end

