defmodule RssReader.Feeds.SlurpSupervisor do
  use Supervisor

  def start_link(arg) do
    Supervisor.start_link(__MODULE__, arg, name: __MODULE__)
  end

  # TODO: https://lorem-rss.herokuapp.com/
  @impl true
  def init(_arg) do
    IO.puts "Starting supervisor"
    children = [
      {RssReader.Feeds.Slurper, ["http://feeds.arstechnica.com/arstechnica/index/",
                                 "http://rss.nytimes.com/services/xml/rss/nyt/Science.xml",
                                 "https://lorem-rss.herokuapp.com/feed"]}
    ]

    Supervisor.init(children, strategy: :one_for_one)
  end
end
